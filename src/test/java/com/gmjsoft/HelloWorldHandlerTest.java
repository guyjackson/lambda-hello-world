package com.gmjsoft;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@ExtendWith(MockitoExtension.class)
public class HelloWorldHandlerTest {

    DemoHandler function;

    @BeforeEach
    void setUp() {
        function = new DemoHandler();
    }

    @Test
    public void handle_request_returns_constant_value() {
        Object result = function.handleRequest("hello world", null);
        assertThat(result).isEqualTo("hello world");
    }
}
